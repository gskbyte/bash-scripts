Bash scripts
============

Utility scripts I use on an almost daily basis to automatize some repetitive work.

Included scripts
----------------

### Android Scaling script ###

Takes files included in an drawable-xhdpi folder and downscales them to "drawable-hdpi" and "drawable-mdpi". It uses ImageMagick's convert tool concurrently.

