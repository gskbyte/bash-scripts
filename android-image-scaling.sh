#!/bin/bash
# This scripts takes images for Android's xhdpi resolution and downscales them to hdpi and mdpi resolutions.
# Uses ImageMagick's "convert" to do it, and is able to convert many files concurrently 

mkdir -p drawable-mdpi
mkdir -p drawable-hdpi

rm -r drawable-mdpi/*
rm -r drawable-hdpi/*

let max_threads=4;
let num_threads=0;

if [ $# -eq 0 ]
then
  echo "Supply maximum number of concurrent processes like this: 'sh convert.sh num_threads', using $max_threads by default"
else
  let max_threads=$1;
  echo "Running up to $max_threads processes"
fi

let count=0;

for img in `ls drawable-xhdpi/*`
do
  let count=count+1;

  # create and resize file
  cut=`basename $img`

  convert $img -filter bessel -resize 67% drawable-hdpi/$cut &
  convert $img -filter bessel -resize 50% drawable-mdpi/$cut &

  # thread control, wait for all processes to finish before launching more
  let num_threads=num_threads+2;
  if [ "$num_threads" -ge "$max_threads" ]
  then
    for job in `jobs -p`
    do
      wait $job
    done
    let num_threads=0;
  fi
done

echo "Finished, $count files processed"
